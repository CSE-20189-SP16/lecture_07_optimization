CC=		gcc
CFLAGS=		-std=c99
SOURCE=		$(wildcard *.c)
PROGRAMS=	$(SOURCE:.c=-O0) \
		$(SOURCE:.c=-Os) \
		$(SOURCE:.c=-O2) \
		$(SOURCE:.c=-O3)

all:	$(PROGRAMS)

%-O0:	%.c
	$(CC) $(CFLAGS) -O0 -o $@ $<

%-Os:	%.c
	$(CC) $(CFLAGS) -Os -o $@ $<

%-O2:	%.c
	$(CC) $(CFLAGS) -O2 -o $@ $<

%-O3:	%.c
	$(CC) $(CFLAGS) -O3 -o $@ $<

clean:
	rm -f $(PROGRAMS)
